Articles = new Mongo.Collection("articles");

if (Meteor.isClient) {

    Template.body.events({
        'click': function(event) {
            if (!$(event.target).closest('.auth-block').length) {
                $('.popup').hide();
            }
        }
    });

    Template.registerHelper('formatDate', function(date) {
        var d = new Date(date);
        return d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate();
    });
}

