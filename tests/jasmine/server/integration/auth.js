describe('application.auth', function() {
    var user, email, userName, password;

    beforeEach(function() {

        email = 'test@test.com';
        userName = 'TestUserName';
        password = 'secretPassword';

        if (user = Accounts.findUserByUsername(userName)) {
            Meteor.users.remove(user._id);
        }
    });

    it('check if user not exist', function(done) {
        // check user not exist
        user = Accounts.findUserByUsername(userName);
        expect(user).toBeUndefined();
        done();
    });

    it('try register new user and check if new user exist already', function(done) {
        // check creating new user
        var userId = Accounts.createUser({username: userName, password: password, email: email});
        expect(userId).not.toBeNull();

        // check user exist already
        user = Accounts.findUserByUsername(userName);
        expect(user).not.toBeUndefined();
        done();
    });

});
