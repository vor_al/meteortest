describe('application.articles', function() {
    var email, userName, password,
        articles;

    beforeEach(function(done) {

        email = 'test@test.com';
        userName = 'TestUserName';
        password = 'secretPassword';

        articles = [{
            title: 'Test article title',
            description: 'A user document can contain any data you want to store about a user. ' +
            'Meteor treats the following fields specially'
        }, {
            title: 'Test article title2',
            description: 'Velocity runs our integration tests in a mirror instance of our app, ' +
            'which also has its own database. '
        }];

        Accounts.createUser({username: userName, password: password, email: email});
        var addedCount = 0;
        Meteor.loginWithPassword(email, password, function () {
            Meteor.call('removeAllArticles', function() {

                articles.forEach(function (data) {
                    Meteor.call('addArticle', data.title, data.description, function() {
                        addedCount++;
                        if (addedCount == 2) done();
                    });
                });
            });
        });


    });

    it('shows a list of the articles', function() {
        expect(Meteor.userId()).not.toBeNull();
        expect($('.articleBlock').length).toEqual(2);
    });

    it('try to add new article', function(done) {

        expect($('.articleBlock').length).toEqual(2);

        Meteor.call('addArticle', articles[0].title, articles[0].description, function() {
            expect($('.articleBlock').length).toEqual(3);
            done();
        });
    });

    it('try to delete article', function(done) {

        expect($('.articleBlock').length).toEqual(2);

        var article = Articles.findOne({title: articles[0].title});

        Meteor.call('deleteArticle', article._id, function() {
            expect($('.articleBlock').length).toEqual(1);
            done();
        });
    });

});
