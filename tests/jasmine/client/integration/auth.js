describe('application.auth', function() {
    var user, email, userName, password, password2;

    beforeEach(function(done) {

        email = 'test@test.com';
        userName = 'TestUserName';
        password = 'secretPassword';
        password2 = 'secretPassword';
        Meteor.logout(function() {
            done();
        });
    });

    it('try login user', function(done) {
        expect(Meteor.userId()).toBeNull();

        Meteor.loginWithPassword(email, password, function(err) {
            expect(err).toBeUndefined();
            expect(Meteor.userId()).not.toBeNull();
            Meteor.logout(function() {
                done();
            });
        });
    });
});
