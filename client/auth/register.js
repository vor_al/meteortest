
Template.register.events({
    'click #show-register': function() {
        $('.popup').not('#register-popup').hide();
        $('#register-popup').toggle();
    },

    'submit form': function(event) {
        event.preventDefault();

        var form = event.target;
        var email = form.registerEmail.value,
            userName = form.registerUsername.value,
            password = form.registerPassword.value,
            password2 = form.registerPassword2.value;
        if (password != password2) {
            $('registerWarning').show();
        } else {
            Accounts.createUser({username: userName, password: password, email: email}, function (error) {
                console.log(error);
            });
        }
    }
});