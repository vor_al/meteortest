
Template.login.events({
    'click #show-login': function() {
        $('.popup').not('#login-popup').hide();
        $('#login-popup').toggle();
    },

    'submit form': function(event) {
        event.preventDefault();

        var form = event.target;
        var email = form.loginEmail.value,
            password = form.loginPassword.value;

        Meteor.loginWithPassword(email, password, function (error) {
            console.log(error);
        });
    }
});