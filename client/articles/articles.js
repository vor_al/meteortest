
Meteor.subscribe('articles');

Template.body.helpers({
    articles: function () {
        return Articles.find();
    }
});

Template.body.events({
    'submit .new-article': function (event) {
        event.preventDefault();

        var title = event.target.articleTitle.value,
            description = event.target.articleDescription.value;

        if (title != '' && description != '') {
            Meteor.call('addArticle', title, description);
            event.target.articleTitle.value = '';
            event.target.articleDescription.value = '';
        } else {
            $('#addWarning').show();
            setTimeout("$('#addWarning').hide()", 3000);
        }
    }
});

Template.article.events({
    'click .delete': function() {
        if (confirm('Delete this article?')) {
            Meteor.call('deleteArticle', this._id);
        }
    }
});