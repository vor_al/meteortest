
Meteor.publish('articles', function () {
    return Articles.find({ owner: this.userId });
});

Meteor.methods({
    addArticle: function(title, description) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('access denied');
        }
        Articles.insert({
            title: title,
            description: description,
            created_date: new Date(),
            owner: Meteor.userId(),
            username: Meteor.user().username
        });
    },

    deleteArticle: function(articleId) {
        var article = Articles.findOne(articleId);

        if (!Meteor.userId() || Meteor.userId() != article.owner) {
            throw new Meteor.Error('access denied');
        }

        Articles.remove(articleId);
    }
});

Meteor.startup(function() {

    return Meteor.methods({
        removeAllArticles: function() {
            return Articles.remove({});
        }
    });

});